import imaplib
import email
import os
import html2text
import time
import hashlib
from datetime import datetime, timedelta
import sys
import mysql.connector
from mysql.connector import Error
from mysql.connector import (connection)
from mysql.connector import pooling

import re
from email.parser import BytesParser
from email.policy import default
import threading
import json
from pytz import timezone
import pytz
import pyodbc
import locale
from flask import Flask
from flask_restful import reqparse, abort, Api, Resource
import logging
import mimetypes
import config

app = Flask(__name__)
api = Api(app)
parser = reqparse.RequestParser()
parser.add_argument('qr_code')
parser.add_argument('area_code')
parser.add_argument('car_number')
parser.add_argument('ma_lenh')
parser.add_argument('ngay_xuat')
parser.add_argument('ma_hang_hoa')


threads = []

class SMO_System:
    def __init__(self):
        self.smo = True

    def get_smo_info(self, qr_code, area_code):

        sql_connection_string = ''
        if area_code == 'ben_thuy':
            sql_connection_string = config.ben_thuy_connection_smo
        if area_code == 'nghi_huong':
            sql_connection_string = config.nghi_huong_connection_smo

        conn = pyodbc.connect(sql_connection_string)
        conn.setencoding(encoding='utf-8')

        cursor = conn.cursor()

        order_info = None
        cursor.execute('SELECT po.PoCode, CONVERT(VARCHAR, po.DocDate, 120) as DocDate , po.OicPBatch, po.OicPtrip, po.VehicleCode, po.DoSAP '
                       ' from PO_Header as po '
                       ' where po.DoSAP = \'' +str(qr_code)+'\'')
        record =  cursor.fetchone()
        columnNames = [column[0] for column in cursor.description]
        if record:
            order_info = dict(zip(columnNames, record))

        order_detail = []
        cursor.execute('SELECT po.PoCode, CONVERT(VARCHAR, po.DocDate, 120) as DocDate , po.OicPBatch, po.OicPtrip, po.VehicleCode,'
                       ' po.DoSAP, pod.Item, RIGHT(pod.MaterialCode,6) as MaterialCode, CONVERT(int,pod.Quantity) as Quantity, p.MaterialText '
                       ' from PO_Header as po, PO_Detail as pod, MD_Material as p'
                       ' where po.DoSAP = \'' +str(qr_code)+'\' and po.PoCode = pod.PoCode and pod.MaterialCode = p.MaterialCode ')

        records = cursor.fetchall()
        columnNames = [column[0] for column in cursor.description]
        for record in records:
            order_detail.append(dict(zip(columnNames, record)))

        data = {}
        data['order_info'] = order_info
        data['order_detail'] = order_detail

        return  data





class TGBX_system:
    def __init__(self):
        self.tgbx = True

    def get_tgbx_info(self,car_number,area_code):
        sql_connection_string = ''
        if area_code == 'ben_thuy':
            sql_connection_string = config.ben_thuy_connection_tgbx
        if area_code == 'nghi_huong':
            sql_connection_string = config.nghi_huong_connection_tgbx

        conn = pyodbc.connect(sql_connection_string)
        conn.setencoding(encoding='utf-8')

        cursor = conn.cursor()

        now = datetime.now()
        ytd  = datetime.today() - timedelta(days=1)

        today = now.strftime("%Y-%m-%d")
        yesterday  = ytd.strftime("%Y-%m-%d")

        if config.env == 'test':
            today = '2021-07-23'
            yesterday = '2021-07-23'
            car_number = '73C12193'

        list_ma_lenh = []
 #       cursor.execute( 'select Malenh, SoLenh, CONVERT(VARCHAR, NgayXuat, 111) as  NgayXuat, MaPhuongTien, NguoiVanChuyen,'
#                        ' Status from [dbo].[tblLenhXuatE5] '/                        ' where (NgayXuat = \''+ today +'\' or NgayXuat = \''+yesterday+'\') '
#                        ' and MaPhuongTien = \''+ car_number+ '\' ')

        cursor.execute( 'select ct.MaTuDongHoa Malenh, e.SoLenh, CONVERT(VARCHAR, e.NgayXuat, 111) as  NgayXuat, e.MaPhuongTien, e.NguoiVanChuyen, rtrim(ltrim(e.Status)) [Status]'
' from Fpt_tblLenhXuatChiTietE5_V ct '
'inner join tblLenhXuatE5 e on ct.SoLenh = e.SoLenh and ct.MaLenh = e.MaLenh and ct.NgayXuat = e.NgayXuat and ct.PhuongTien = e.MaPhuongTien '
' where e.MaPhuongTien = \'' + car_number + '\' and (e.NgayXuat = \'' + today + '\')') 
#' where e.MaPhuongTien = \'' + car_number + '\' and (e.NgayXuat = \'' + today + '\' or e.NgayXuat = \'' + yesterday + '\')')

        records = cursor.fetchall()
        columnNames = [column[0] for column in cursor.description]
        for record in records:
            list_ma_lenh.append(dict(zip(columnNames, record)))

        data = {}

        list_order = []
        order_info = None
        order_detail = []

        if len(list_ma_lenh) == 0:
            data['list_order'] = list_order
            return data

        tdh = TDH_system(area_code)
        for ma_lenh in list_ma_lenh:
            print(ma_lenh)
            list_ma_lenh_detail = []
#            cursor.execute('select MaLenh, SoLenh, LineID, CONVERT(int,TongDuXuat)  as TongDuXuat, RIGHT(MaHangHoa,6) as MaHangHoa from tblLenhXuat_HangHoaE5 where  SoLenh = ? ', (ma_lenh['SoLenh'],))
            cursor.execute('select MaTuDongHoa MaLenh, SoLenh, LineID, CONVERT(int,SoLuongDuXuat)  as TongDuXuat, RIGHT(MaHangHoa,6) as MaHangHoa from Fpt_tblLenhXuatChiTietE5_V where  SoLenh = ? ', (ma_lenh['SoLenh'],))
	  # cursor.execute('select MaTuDongHoa MaLenh, SoLenh, LineID, CONVERT(int,SoLuongDuXuat) as TongDuXuat, RIGHT(MaHangHoa,6) as MaHangHoa from Fpt_tblLenhXuatChiTietE5_V where  SoLenh = ? ', (ma_lenh['SoLenh'],))
            records = cursor.fetchall()
            columnNames = [column[0] for column in cursor.description]
            for record in records:
                list_ma_lenh_detail.append(dict(zip(columnNames, record)))

            for ma_lenh_detail in list_ma_lenh_detail:
                trang_thai = tdh.get_tdh_info(ma_lenh['MaPhuongTien'],ma_lenh['Malenh'],
                                              ma_lenh['NgayXuat'],ma_lenh_detail['MaHangHoa'])
                print("cannt", trang_thai)
#		print("cannt", trang_thai)
#		print("cannt3", trang_thai)
                if(not trang_thai) or trang_thai == 'dang_cho':
                    order_info = ma_lenh
                    order_detail = list_ma_lenh_detail
                    order_data = {
                        'order_info' : order_info,
                        'order_detail' : order_detail
                    }
                    list_order.append(order_data)
                    break

                if config.env == 'dev' and trang_thai == 'ket_thuc':
                    order_info = ma_lenh
                    order_detail = list_ma_lenh_detail
                    order_data = {
                        'order_info': order_info,
                        'order_detail': order_detail
                    }
                    list_order.append(order_data)
                    break

        data['list_order'] = list_order
        print('get_tgbx_info',data)
        return data



    def scan_tgbx_info(self,car_number,ma_lenh,ngay_xuat,ma_hang_hoa,area_code):
        tdh = TDH_system(area_code)
        return tdh.get_tdh_info(car_number,ma_lenh,ngay_xuat,ma_hang_hoa)

class TDH_system:
    def __init__(self, area_code):
        self.tdh = True
        self.area_code = area_code

    def get_tdh_info(self, ma_phuong_tien, malenh, ngay_xuat, ma_hang_hoa):
        if self.area_code == 'nghi_huong':
            return self.get_tdh_nghi_huong_info(ma_phuong_tien,malenh,ngay_xuat,ma_hang_hoa)

        if self.area_code == 'ben_thuy':
            return self.get_tdh_ben_thuy_info( ma_phuong_tien, malenh, ngay_xuat, ma_hang_hoa)

    def get_tdh_nghi_huong_info(self, ma_phuong_tien, malenh, ngay_xuat, ma_hang_hoa):
        malenh = int(malenh)
        ma_hang_hoa = ma_hang_hoa[-6:]

        sql_connection_tdh = config.nghi_huong_connection_tdh
        
        conn_tdh = pyodbc.connect(sql_connection_tdh)
        conn_tdh.setencoding(encoding='utf-8')
        cursor = conn_tdh.cursor()
        cursor.execute('select Trang_thai_lenh TrangThai from BX_BangMaLenh where '
                       'Ma_lenh = ? and Time_tao_lenh = ? and So_ptien = ? '
                       ' and RIGHT(RTRIM(Ma_hang),6) = ? ',(malenh, ngay_xuat, ma_phuong_tien, ma_hang_hoa))
        record = cursor.fetchone()
        columnNames = [column[0] for column in cursor.description]
        if record:
            data = dict(zip(columnNames, record))
            print("cannt_check trang thai1: ", str(data['TrangThai']))
            if str(data['TrangThai']) == 'KT':
                return 'ket_thuc'
            elif str(data['TrangThai']) == 'DX':
                return 'ket_thuc'
            elif str(data['TrangThai']) == 'OK':
                return 'dang_cho'
            else:
                return 'khong_ro'
        else:
            cursor.execute('select TRANG_THAI from Lenh_GH where '
                           'MA_LENH = ? and NGAY_DKY = ? and SO_PTIEN = ? '
                           ' and RIGHT(RTRIM(MaHangHoa),6) = ? ', (malenh, ngay_xuat, ma_phuong_tien, ma_hang_hoa))
            record = cursor.fetchone()
            columnNames = [column[0] for column in cursor.description]
            if record:
                data = dict(zip(columnNames, record))
                print("cannt_check trang thai2: ", str(data['TRANG_THAI']))
                if str(data['TRANG_THAI']) == '4': #ket thuc khi an print
                    return 'ket_thuc'
                if str(data['TRANG_THAI']) == '3': #ket thuc sau 3 phut khong thao tac
                    return 'ket_thuc'
                elif str(data['TRANG_THAI']) == '2': #dang xuat
                    return 'ket_thuc'
                elif str(data['TRANG_THAI']) == '0': #chưa xuất
                    return 'dang_cho'
                else:
                    return 'khong_ro'
            else:
                return ''

        return ''

    def get_tdh_ben_thuy_info(self, ma_phuong_tien, malenh, ngay_xuat, ma_hang_hoa):
        malenh = int(malenh)
        ma_hang_hoa = ma_hang_hoa[-6:]
        sql_connection_tdh = config.ben_thuy_connection_tdh
        conn_tdh = pyodbc.connect(sql_connection_tdh)
        conn_tdh.setencoding(encoding='utf-8')
        cursor = conn_tdh.cursor()

        cursor.execute('select TrangThai from tblLenhXuatChiTiet where '
                       'Malenh = ? and NgayXuat = ? and MaPhuongTien = ? '
                       ' and RIGHT(RTRIM(MaHangHoa),6) = ? ',(malenh, ngay_xuat, ma_phuong_tien, ma_hang_hoa))
        record = cursor.fetchone()
        columnNames = [column[0] for column in cursor.description]
        if record:
            data = dict(zip(columnNames, record))
            print("cannt_check trang thai1: ", str(data['TrangThai']))
            if str(data['TrangThai']) == 'KT':
                return 'ket_thuc'
            elif str(data['TrangThai']) == 'DX':
                return 'ket_thuc'
            elif str(data['TrangThai']) == 'OK':
                return 'dang_cho'
            else:
                return 'khong_ro'
        return ''


class ProcessSMO(Resource):
    def post(self):
        args = parser.parse_args()
        qr_code = args['qr_code']
        area_code = args['area_code']

        smo = SMO_System()
        return smo.get_smo_info(qr_code,area_code)

class ProcessTGBX_ListOrder(Resource):
    def post(self):
        args = parser.parse_args()
        car_number = args['car_number']
        area_code = args['area_code']

        tgbx = TGBX_system()

        return tgbx.get_tgbx_info(car_number,area_code)

class ProcessTDH_CheckStatus(Resource):
    def post(self):
        args = parser.parse_args()
        car_number = args['car_number']
        ma_lenh = args['ma_lenh']
        ngay_xuat = args['ngay_xuat']
        ma_hang_hoa = args['ma_hang_hoa']

        area_code = args['area_code']

        tdh = TDH_system(area_code)

        return tdh.get_tdh_info(car_number,ma_lenh,ngay_xuat,ma_hang_hoa)

api.add_resource(ProcessSMO, '/smo/')
api.add_resource(ProcessTGBX_ListOrder, '/tgbx/get_order/')
api.add_resource(ProcessTDH_CheckStatus, '/tdh/check_status/')

if __name__ == '__main__':


    app.run(host='0.0.0.0', port=8001, threaded=True)

    # conn = pyodbc.connect('Driver={SQL Server};'
    #                       'Server=tcp:sso.d2s.com.vn,1433;'
    #                       'Database=SMO;'
    #                       'UId=vcs;PWD=vcs@2021;')
    #
    # cursor = conn.cursor()
    # cursor.execute('SELECT TOP (1000) [IDTable],[ContractCode],[ContractType],[SaleOrg],[DCCode]'
    #                '  FROM [SMO].[dbo].[MD_Contract]')
    #
    # records = cursor.fetchall()
    # insertObject = []
    # columnNames = [column[0] for column in cursor.description]
    #
    # for record in records:
    #     insertObject.append(dict(zip(columnNames, record)))
    # print(insertObject)
    # print('ok')
    # smo = SMO_System()
    # print(smo.get_smo_info('2002303017'))
